package com.cristhianwiki.afinal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bosphere.filelogger.FL;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static String TAG = "Aplicación FINAL";

    TextView textViewDescription;
    Button btnVerdadero, btnFalso, btnSiguiente, btnAnterior;
    ImageView imageView;
    RelativeLayout relativeLayout;

    int currentIndex = 0;
    Pregunta pregunta;
    List<Pregunta> preguntas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                    this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1
            );
        }

        FL.i(TAG, "Se ha creado la aplicación");
        setContentView(R.layout.activity_main);

        btnVerdadero = findViewById(R.id.btn_verdadero);
        btnFalso = findViewById(R.id.btn_falso);
        btnAnterior = findViewById(R.id.btn_anterior);
        btnSiguiente = findViewById(R.id.btn_siguiente);
        imageView = findViewById(R.id.image_current);
        textViewDescription = findViewById(R.id.description);
        relativeLayout = findViewById(R.id.relativeLayout);

        preguntas = Pregunta.obtenerDatos(this);

        initPregunta();

        btnSiguiente.setOnClickListener(v -> siguientePregunta());
        btnAnterior.setOnClickListener(v -> anteriorPregunta());
        btnVerdadero.setOnClickListener(v -> marcarPregunta(currentIndex, true));
        btnFalso.setOnClickListener(v -> marcarPregunta(currentIndex, false));
    }

    public void initPregunta(){
        pregunta = preguntas.get(currentIndex);
        FL.i(TAG, "Inicializando pregunta: " + pregunta.getDescripcion());
        textViewDescription.setText(pregunta.getDescripcion());
        imageView.setImageResource(pregunta.getResId());
    }

    public void siguientePregunta(){
        btnVerdadero.setEnabled(true);
        btnFalso.setEnabled(true);

        currentIndex++;
        if(currentIndex >= preguntas.size()){
            currentIndex = 0;
        }

        pregunta = preguntas.get(currentIndex);
        FL.i(TAG, "Se va a la siguiente pregunta: " + pregunta.getDescripcion());
        textViewDescription.setText(pregunta.getDescripcion());
        imageView.setImageResource(pregunta.getResId());
    }

    public void anteriorPregunta(){
        btnVerdadero.setEnabled(true);
        btnFalso.setEnabled(true);
        currentIndex--;
        if(currentIndex < 0){
            currentIndex = 0;
        }
        pregunta = preguntas.get(currentIndex);
        FL.i(TAG, "Se va a la anterior pregunta: " + pregunta.getDescripcion());
        textViewDescription.setText(pregunta.getDescripcion());
        imageView.setImageResource(pregunta.getResId());
    }

    public void marcarPregunta(int index, Boolean isCorrect){
        btnVerdadero.setEnabled(false);
        btnFalso.setEnabled(false);
        pregunta = preguntas.get(index);
        if(isCorrect == pregunta.getIsCorrect()){
            Snackbar.make(relativeLayout, R.string.msg_snackbar_correcta, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.button_snackbar_text, v -> {

                    }).show();
            FL.i(TAG, "El usuario marcó " + isCorrect + " para la pregunta " + (index+1) + ": " +
                            pregunta.getDescripcion() + " (CORRECTO)"
                    );
        }else{
            Snackbar.make(relativeLayout, R.string.msg_snackbar_incorrecta, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.button_snackbar_text, v -> {

                    }).show();
            FL.i(TAG, "El usuario marcó " + isCorrect + " para la pregunta " + (index+1) + ": " +
                    pregunta.getDescripcion() + " (INCORRECTO)"
            );
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        FL.i(TAG, "La aplicación se ha pausado");
    }

    @Override
    protected void onStop() {
        super.onStop();
        FL.i(TAG, "La aplicación se ha detenido");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FL.i(TAG, "La aplicación se ha cerrado");
    }

}