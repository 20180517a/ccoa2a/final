package com.cristhianwiki.afinal;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;

public class Pregunta {
    private int resId;
    private Boolean isCorrect;
    private String descripcion;

    public Pregunta() {
    }

    public Pregunta(Integer resId, Boolean isCorrect, String descripcion) {
        this.resId = resId;
        this.isCorrect = isCorrect;
        this.descripcion = descripcion;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(Integer resId) {
        this.resId = resId;
    }

    public Boolean getIsCorrect() {
        return isCorrect;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public static List<Pregunta> obtenerDatos(Context context){
        List<Pregunta> preguntas = new ArrayList<>();
        preguntas.add(new Pregunta(R.drawable.aji, true, context.getString(R.string.pregunta1)));
        preguntas.add(new Pregunta(R.drawable.arroz_con_pollo, false, context.getString(R.string.pregunta2)));
        preguntas.add(new Pregunta(R.drawable.carapulcra, true, context.getString(R.string.pregunta3)));
        preguntas.add(new Pregunta(R.drawable.ceviche, false, context.getString(R.string.pregunta4)));
        preguntas.add(new Pregunta(R.drawable.causa, true, context.getString(R.string.pregunta5)));
        preguntas.add(new Pregunta(R.drawable.lomo_saltado, true, context.getString(R.string.pregunta6)));
        preguntas.add(new Pregunta(R.drawable.pollo_brasa, false, context.getString(R.string.pregunta7)));
        preguntas.add(new Pregunta(R.drawable.tacu_tacu, false, context.getString(R.string.pregunta8)));
        return preguntas;
    }
}
